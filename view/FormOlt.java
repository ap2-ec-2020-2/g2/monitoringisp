package view;

import javax.swing.*;
import java.awt.event.WindowAdapter;
import java.awt.BorderLayout;
import java.awt.FlowLayout;

import java.awt.event.WindowEvent;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;
import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import java.awt.GridLayout;
import java.awt.Dimension;
import java.awt.event.WindowEvent;
import javax.swing.border.TitledBorder;
import javax.swing.border.LineBorder;
import java.awt.Color;

import model.entity.Olt;
import model.Database;
import controller.OltController;

import view.util.CrudToolBar;
import view.util.FormUtils;

public class FormOlt extends FormEntidade
{
    private JFrame frame;
    private JFrame formPai;
    private OltController controller;
    private Database database;
    private List<Olt> oltList;

    private CrudToolBar crudToolBar;

    private JPanel painelPrincipal;
    private JPanel subPainel_1;
    private JPanel subPainel_2;

    private JTextField txtId;
    private JTextField txtNome;
    private JTextField txtCargaHoraria;
    
    private JButton botaoVoltar;

    public FormOlt (JFrame form, Database database, String n)
    {
        formPai = form;
        this.controller = new OltController(database);
        setAppNome(n);
    }    

    public void criarExibirForm()
    {       
        // Criando e configurando a janela do formulario.
        frame = new JFrame(getAppNome());        
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        painelPrincipal = new JPanel();            
        painelPrincipal.setLayout(new GridLayout(3,1));        
        crudToolBar = new CrudToolBar(this);        
        painelPrincipal.add(crudToolBar);

        subPainel_1 = new JPanel(); 
        subPainel_1.setBorder(new TitledBorder(
         new LineBorder(Color.BLACK), "Disciplina:"));        
        subPainel_1.setLayout(new GridBagLayout());         
        GridBagConstraints gbc = new GridBagConstraints();
        gbc.fill = GridBagConstraints.HORIZONTAL;
        gbc.insets = new Insets(3,3,3,3);   

        // ID
        gbc.gridx = 0;
        gbc.gridy = 0;
        gbc.gridwidth = 1;                  
        subPainel_1.add(new JLabel("ID:"),gbc);             
        txtId = new JTextField();
        txtId.setPreferredSize(new Dimension(140,19));
        txtId.setEnabled(false);
        gbc.gridx = 1;
        gbc.gridy = 0;
        gbc.gridwidth = 1;          
        subPainel_1.add(txtId);                     

        // Nome
        gbc.gridx = 0;
        gbc.gridy = 1;
        gbc.gridwidth = 1;                  
        subPainel_1.add(new JLabel("Nome:"),gbc);             
        txtNome = new JTextField();
        gbc.gridx = 1;
        gbc.gridy = 1;
        gbc.gridwidth = 1;                  
        subPainel_1.add(txtNome,gbc);             

        // Carga Horaria
        gbc.gridx = 0;
        gbc.gridy = 2;
        gbc.gridwidth = 1;                  
        subPainel_1.add(new JLabel("Carga Horaria:"),gbc);             
        txtCargaHoraria = new JTextField();
        gbc.gridx = 1;
        gbc.gridy = 2;
        gbc.gridwidth = 1;                  
        subPainel_1.add(txtCargaHoraria,gbc);         

        painelPrincipal.add(subPainel_1);
        
        subPainel_2 = new JPanel(); 
        subPainel_2.setLayout(new FlowLayout( FlowLayout.RIGHT,2,2)); 
        botaoVoltar = new JButton("Voltar"); 
        subPainel_2.add(botaoVoltar);
        botaoVoltar.addActionListener(this);
        
        painelPrincipal.add(subPainel_2);
        
        frame.add(painelPrincipal);

        frame.pack();
        FormUtils.centerForm(frame);                
        frame.setVisible(true);

        lerEntidades();
        exibirEntidade(getNumEntidadeCorrente());
    }

    public void actionPerformed(ActionEvent e) 
    {
       if(e.getSource().equals(botaoVoltar))
        {        
            buttonVoltar_Click();
        }
    }

    public Disciplina getEntidadeCorrente()
    {
        Disciplina disciplina = new Disciplina();
        int id, ch;

        if(txtId.getText().isEmpty() == false)
        {
            id = Integer.parseInt(txtId.getText());
            disciplina.setId(id);
        }

        if(txtNome.getText().isEmpty() == false)
        {
            disciplina.setNome(txtNome.getText());
        }

        if(txtCargaHoraria.getText().isEmpty() == false)
        {
            ch = Integer.parseInt(txtCargaHoraria.getText());
            disciplina.setCargaHoraria(ch);
        }

        return disciplina;
    }

    public void exibirEntidade(int index)
    {
        Disciplina disciplina;

        if (index == -1)
        {
            FormUtils.clearTextFields(this,subPainel_1);    
        }
        else
        {
            disciplina = oltList.get(index);
            txtId.setText(Integer.toString(disciplina.getId()));
            txtNome.setText(disciplina.getNome());  
            txtCargaHoraria.setText(Integer.toString(disciplina.getCargaHoraria()));
        }
    }

    public void salvarEntidade()
    {
        Disciplina disciplina;

        disciplina = getEntidadeCorrente();        
        Disciplina disciplinaCriada = controller.create(disciplina);
    }

    public void alterarEntidade()
    {
        Disciplina disciplina = new Disciplina();

        disciplina = getEntidadeCorrente();

        controller.update(disciplina);

        lerEntidades();
        exibirEntidade(getNumEntidadeCorrente());
    }

    public void deletarEntidade()
    {
        Disciplina disciplina = new Disciplina();

        disciplina = getEntidadeCorrente();

        controller.delete(disciplina);

        lerEntidades();
        exibirEntidade(getNumEntidadeCorrente());
    }

    public void lerEntidades()
    {
        oltList = controller.readAll();     

        if(oltList.isEmpty())
        {
            setNumEntidadeCorrente(-1);
        }
        else
        {
            setNumEntidadeCorrente(0);            
        }
    }  

    public int totalEntidades()
    {
        return oltList.size();
    }

    public JPanel getSubPainelCampos()
    {
        return subPainel_1;
    }
    
    private void buttonVoltar_Click()
    {
        frame.setVisible(false);
        formPai.setEnabled(true);
        frame.dispose();
    }
}
