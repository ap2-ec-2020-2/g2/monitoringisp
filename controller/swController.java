package controller;

import java.util.List;
import java.util.ArrayList;

import model.entity.Sw;
import model.SwDao;
import model.SwDao;
import model.Database;

public class swController 
{
    private SwDao dao;

    public swController(Database database)
    {
        this.dao = new SwDao(database);
    }

    public Sw create(Sw Sw) 
    {
        return dao.create(Sw);
    }    

    public List<Sw> readAll() 
    {
        return dao.readAll();
    }

    public void update(Sw Sw) 
    {
        dao.update(Sw);
    }

    public void delete(Sw Sw) 
    {
        dao.delete(Sw);
    }  
    
}