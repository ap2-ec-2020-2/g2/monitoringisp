package controller;

import java.util.List;
import java.util.ArrayList;

import model.entity.concentrador;
import model.concentradorDao;
import model.Database;

public class concentradorController 
{
    private concentradorDao dao;

    public concentradorController(Database database)
    {
        this.dao = new concentradorDao(database);
    }

    public concentrador create(concentrador concentrador) 
    {
        return dao.create(concentrador);
    }    

    public List<concentrador> readAll() 
    {
        return dao.readAll();
    }

    public void update(concentrador concentrador) 
    {
        dao.update(concentrador);
    }

    public void delete(concentrador concentrador) 
    {
        dao.delete(concentrador);
    }  
    
}