package controller;

import java.util.List;
import java.util.ArrayList;

import model.entity.Olt;
import model.OltDao;
import model.Database;

public class OltController 
{
    private OltDao dao;

    public OltController(Database database)
    {
        this.dao = new OltDao(database);
    }

    public Olt create(Olt Olt) 
    {
        return dao.create(Olt);
    }    

    public List<Olt> readAll() 
    {
        return dao.readAll();
    }

    public void update(Olt Olt) 
    {
        dao.update(Olt);
    }

    public void delete(Olt Olt) 
    {
        dao.delete(Olt);
    }  
    
}