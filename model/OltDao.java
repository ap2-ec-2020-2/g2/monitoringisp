package model;

import com.j256.ormlite.dao.DaoManager;
import com.j256.ormlite.dao.Dao;
import java.sql.SQLException;
import com.j256.ormlite.table.TableUtils;
import java.util.List;
import java.util.ArrayList;

import model.entity.Olt;

public class OltDao
{
    private static Database database;
    private static Dao<Olt, Integer> dao;
    private List<Olt> oltsCarregadas;
    private Olt oltCarregada; 

    public OltDao(Database database) 
    {
        this.setDatabase(database);
        oltsCarregadas = new ArrayList<Olt>();
    }

    public static void setDatabase(Database database) 
    {
        OltDao.database = database;
        try {
            dao = DaoManager.createDao(database.getConnection(), Olt.class);
            TableUtils.createTableIfNotExists(database.getConnection(), Olt.class);
        }
        catch(SQLException e) {
            System.out.println(e);
        }            
    } 

    public Olt create(Olt olt) 
    {
        int nrows = 0;
        try {
            nrows = dao.create(olt);
            if ( nrows == 0 )
                throw new SQLException("Erro ao salvar o objeto.");
            this.oltCarregada = olt;
            oltsCarregadas.add(olt);
        } catch (SQLException e) {
            System.out.println(e);
        }
        return olt;
    } 

    public Olt readFromId(int id) 
    {
        try {
            this.oltCarregada = dao.queryForId(id);
            if (this.oltCarregada != null)
                this.oltsCarregadas.add(this.oltCarregada);
        } catch (SQLException e) {
            System.out.println(e);
        }
        return this.oltCarregada;
    }    

    public List<Olt> readAll() 
    {
        try {
            this.oltsCarregadas = dao.queryForAll();
            if (this.oltsCarregadas.size() != 0)
                this.oltCarregada = this.oltsCarregadas.get(0);
        } catch (SQLException e) {
            System.out.println(e);
        }
        return this.oltsCarregadas;
    }  

    public void update(Olt olt) 
    {
        try
        {
            dao.update(olt);
        }
        catch (java.sql.SQLException e)
        {
            System.out.println(e);
        }
    }

    public void delete(Olt olt) 
    {
        try
        {
            dao.deleteById(olt.getId());
        }
        catch (SQLException e)
        {
            System.out.println(e);
        }
    }  
}
