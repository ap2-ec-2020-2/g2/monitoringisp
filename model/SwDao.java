package model;

import com.j256.ormlite.dao.DaoManager;
import com.j256.ormlite.dao.Dao;
import java.sql.SQLException;
import com.j256.ormlite.table.TableUtils;
import java.util.List;
import java.util.ArrayList;

import model.entity.Sw;

public class SwDao
{
    private static Database database;
    private static Dao<Sw, Integer> dao;
    private List<Sw> swCarregados;
    private Sw swCarregado; 

    public SwDao(Database database) 
    {
        this.setDatabase(database);
        swCarregados = new ArrayList<Sw>();
    }

    public static void setDatabase(Database database) 
    {
        SwDao.database = database;
        try {
            dao = DaoManager.createDao(database.getConnection(), Sw.class);
            TableUtils.createTableIfNotExists(database.getConnection(), Sw.class);
        }
        catch(SQLException e) {
            System.out.println(e);
        }            
    } 

    public Sw create(Sw sw) 
    {
        int nrows = 0;
        try {
            nrows = dao.create(sw);
            if ( nrows == 0 )
                throw new SQLException("Erro ao salvar o objeto.");
            this.swCarregado = sw;
            swCarregados.add(sw);
        } catch (SQLException e) {
            System.out.println(e);
        }
        return sw;
    } 

    public Sw readFromId(int id) 
    {
        try {
            this.swCarregado = dao.queryForId(id);
            if (this.swCarregado != null)
                this.swCarregados.add(this.swCarregado);
        } catch (SQLException e) {
            System.out.println(e);
        }
        return this.swCarregado;
    }    

    public List<Sw> readAll() 
    {
        try {
            this.swCarregados = dao.queryForAll();
            if (this.swCarregados.size() != 0)
                this.swCarregado = this.swCarregados.get(0);
        } catch (SQLException e) {
            System.out.println(e);
        }
        return this.swCarregados;
    }  

    public void update(Sw sw) 
    {
        try
        {
            dao.update(sw);
        }
        catch (java.sql.SQLException e)
        {
            System.out.println(e);
        }
    }

    public void delete(Sw sw) 
    {
        try
        {
            dao.deleteById(sw.getId());
        }
        catch (SQLException e)
        {
            System.out.println(e);
        }
    }  
}
