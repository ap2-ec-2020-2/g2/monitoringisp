package model;

import com.j256.ormlite.dao.DaoManager;
import com.j256.ormlite.dao.Dao;
import java.sql.SQLException;
import com.j256.ormlite.table.TableUtils;
import java.util.List;
import java.util.ArrayList;

import model.entity.concentrador;

public class concentradorDao
{
    private static Database database;
    private static Dao<concentrador, Integer> dao;
    private List<concentrador> cCarregados;
    private concentrador cCarregado; 

    public concentradorDao(Database database) 
    {
        this.setDatabase(database);
        cCarregados = new ArrayList<concentrador>();
    }

    public static void setDatabase(Database database) 
    {
        concentradorDao.database = database;
        try {
            dao = DaoManager.createDao(database.getConnection(), concentrador.class);
            TableUtils.createTableIfNotExists(database.getConnection(), concentrador.class);
        }
        catch(SQLException e) {
            System.out.println(e);
        }            
    } 

    public concentrador create(concentrador concentrador) 
    {
        int nrows = 0;
        try {
            nrows = dao.create(concentrador);
            if ( nrows == 0 )
                throw new SQLException("Erro ao salvar o objeto.");
            this.cCarregado = concentrador;
            cCarregados.add(concentrador);
        } catch (SQLException e) {
            System.out.println(e);
        }
        return concentrador;
    } 

    public concentrador readFromId(int id) 
    {
        try {
            this.cCarregado = dao.queryForId(id);
            if (this.cCarregado != null)
                this.cCarregados.add(this.cCarregado);
        } catch (SQLException e) {
            System.out.println(e);
        }
        return this.cCarregado;
    }    

    public List<concentrador> readAll() 
    {
        try {
            this.cCarregados = dao.queryForAll();
            if (this.cCarregados.size() != 0)
                this.cCarregado = this.cCarregados.get(0);
        } catch (SQLException e) {
            System.out.println(e);
        }
        return this.cCarregados;
    }  

    public void update(concentrador concentrador) 
    {
        try
        {
            dao.update(concentrador);
        }
        catch (java.sql.SQLException e)
        {
            System.out.println(e);
        }
    }

    public void delete(concentrador concentrador) 
    {
        try
        {
            dao.deleteById(concentrador.getId());
        }
        catch (SQLException e)
        {
            System.out.println(e);
        }
    }  
}
