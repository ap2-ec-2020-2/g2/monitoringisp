package model.entity;

import com.j256.ormlite.table.DatabaseTable;
import com.j256.ormlite.field.DatabaseField;

import javax.security.auth.callback.UnsupportedCallbackException;

import com.j256.ormlite.field.DataType;

@DatabaseTable(tableName = "concentrador")
public class concentrador
{
    //campos da tabela concentrador
    
    @DatabaseField(generatedId = true)
    private int id;

    @DatabaseField
    private String nameEq;

    @DatabaseField
    private String ipAddr;

    @DatabaseField
    private String acTime;
    
    @DatabaseField
    private String date;
    
    @DatabaseField
    private String hour;

    @DatabaseField
    private String loginClient;
    
    @DatabaseField
    private int connectedClients;

    @DatabaseField
    private int disconectedClients;

    @DatabaseField
    private float linkDownloadUsage;

    @DatabaseField
    private float linkUploadUsage;


//set


    public void setId(int i)
    {
        id = i;
    }

    public void setNameEq(String n)
    {
        nameEq = n;
    }

    public void setIpAddr(String ip)
    {
        ipAddr = ip;
    }

    public void setAcTime(String at)
    {
        acTime = at;
    }

    public void setdDate(String dt)
    {
        date = dt;
    }

    public void setHour(String h)
    {
        hour = h;
    }

    public void setLoginClient(String l)
    {
        loginClient = l;
    }

    public void setConnectedClients(int cc)
    {
        connectedClients = cc;
    }

    public void setDisconectedClients(int dc)
    {
        disconectedClients = dc;
    }

    public void setLinkDownloadUsage(float du)
    {
        linkDownloadUsage = du;
    }

    public void setLinkUploadUsage(float uu)
    {
        linkUploadUsage = uu;
    }
    //get
    
    public int getId()
    {
        return id;
    }

    public String getNameEq()
    {
        return nameEq;
    }

    public String getIpAddr()
    {
        return ipAddr;
    }

    public String getAcTime()
    {
        return acTime;
    }

   
    public String getDate()
    {
        return date;
    }

    public String getHour()
    {
        return hour;
    }

    public String getloginClient()
    {
        return loginClient;
    }

    public int getConnectedClients()
    {
        return connectedClients;
    }

    public int getDisconectedClients()
    {
        return disconectedClients;
    }

    public float getLinkDownloadUsage()
    {
        return linkDownloadUsage;
    }
 
    public float getLinkUploadUsage()
    {
        return linkUploadUsage;
    }
}
