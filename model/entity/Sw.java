package model.entity;

import com.j256.ormlite.table.DatabaseTable;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.field.DataType;

@DatabaseTable(tableName = "switch")
public class Sw
{
    //campos da tabela Switch
    
    @DatabaseField(generatedId = true)
    private int id;

    @DatabaseField
    private String nameEq;

    @DatabaseField
    private String ipAddr;

    @DatabaseField
    private String acTime;
    
    @DatabaseField
    private String date;
    
    @DatabaseField
    private String hour;
    
    @DatabaseField
    private int portState;
    
    @DatabaseField
    private float portTx;

    @DatabaseField
    private float portRx;

//set


    public void setId(int i)
    {
        id = i;
    }

    public void setNameEq(String n)
    {
        nameEq = n;
    }

    public void setIpAddr(String ip)
    {
        ipAddr = ip;
    }

    public void setAcTime(String at)
    {
        acTime = at;
    }

    public void setdDate(String dt)
    {
        date = dt;
    }

    public void setdHour(String h)
    {
        hour = h;
    }

    public void setPortState(int ps)
    {
        portState = ps;
    }

    public void setPortTx(float ptx)
    {
        portTx = ptx;
    }
    
    public void setPortRx(float prx)
    {
        portRx = prx;
    }
    //get
    
    public int getId()
    {
        return id;
    }

    public String getNameEq()
    {
        return nameEq;
    }

    public String getIpAddr()
    {
        return ipAddr;
    }

    public String getAcTime()
    {
        return acTime;
    }

   
    public String getDate()
    {
        return date;
    }

    public String getHour()
    {
        return hour;
    }

    public int getPortState()
    {
        return portState;
    }

    public float getPortTx()
    {
        return portTx;
    }

    public float getPortRx()
    {
        return portRx;
    }
}
