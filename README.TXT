----------------------------------------------------------------------------
****IMPORTANTE****

Para uma boa execução do projeto, é recomendado utilizar a IDE IntelliJ, onde o mesmo foi construído.
Todas as bibliotecas externas utilizadas para o desenvolvimento e execução do programa, podem ser encontradas na pasta (lib). É preciso adicionar todas na estrutura do projeto.
A classe App é a que deve ser executada. 
Uma tela de login será apresentada e como senha para acesso foi definido o inteiro (1234).

Caso necessário, o projeto completo também está compactado em um arquivo.zip (PROJETO_grupo2.zip).

------------------------------------------------------------------------

O intuito do projeto é desenvolver um sistema capaz de fornecer as ferramentas necessárias para o monitoramento e manutenção do bom funcionamento da estrutura de um provedor de acesso à internet, como monitorar a quantidade de clientes conectados e seu consumo.

INTEGRANTES:
Yan Freire Caser (@yancaser)
Vittor Gomes de Lima (@vittorlima)
David Wilkerson de Oliveira Silva (@davidwilkerson)
Rhuan Fellipe de Souza Cardoso (@Rhuan.Fellipe)
Wender Charliton Conceição Silva (@wender_charliton)
